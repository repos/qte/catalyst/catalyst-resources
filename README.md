A (community) maintained list of Catalyst resources.

## Vocab

| Term      | Definition |
| --------  | ---------- |
| Component | Something that catalyst knows how to deploy. Can be a service, an extension, a skin, or mediawiki |
| Preset    | A set of resources that are known to work together to make a valid test environment |
